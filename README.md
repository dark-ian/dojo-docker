# Bienvenue sur le Dojo Docker

Ce package contient un squelette pour les exercices. Des fichiers sont déjà 
remplis, d’autres ont des trous.

Surtout, pour ceux qui dérouleront le Dojo sous l’environnement Cloud9, ce 
package inclus un petit proxy qui permet d’accéder depuis le navigateur de votre 
PC aux ressources Docker. (Malgré le système de sécurité d’Amazon)

## Installation du proxy
* `cd docker-dojo`
* `npm i`

## Utilisation du proxy

D’abord, lancer le proxy.

`node proxy.js`

Puis faire `Preview / Preview running application`, puis, pour plus de confort, 
le bouton à droite du la barre d’addresse : `Pop out into new window`

Votre navigateur se lance sur une adresse du type : 
https://3a58e8d1f09047a89e29a121e6f34fdb.vfs.cloud9.eu-west-1.amazonaws.com/

#### A noter que :
* Cette adresse change à chaque connexion
* Cette adresse ne marche que depuis le navigateur où Cloud9 est chargé
* Si la VM derrière Cloud9 tombe en veille, cette adresse est désactivée

Ensuite si vous vouliez accéder à 

http://172.17.0.4:80/index.html

il faut modifier l’adresse comme suit :

https://3a58e8d1f09047a89e29a121e6f34fdb.vfs.cloud9.eu-west-1.amazonaws.com/172.17.0.4/80/index.html 
