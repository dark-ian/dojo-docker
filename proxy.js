var http = require('http'),
  httpProxy = require('http-proxy');

//
// Create a proxy server with custom application logic
//
var proxy = httpProxy.createProxyServer({});

//
// Create your custom server and just call `proxy.web()` to proxy
// a web request to the target passed in the options
// also you can use `proxy.ws()` to proxy a websockets request
//
var server = http.createServer(function(req, res) {
  // You can define here your custom logic to handle the request
  // and then proxy the request.
  const regex = /\/(.+)\/([0-9]+)(\/.*)/g;
  let m;

  console.log(req.url);

  m = regex.exec(req.url);

  if (m === null) {
    // TODO: What happens when it fails ?
    res.writeHead(500, {
      'Content-Type': 'text/plain'
    });

    res.end('${req.url} is invalid');
    return;
  }

  // This is necessary to avoid infinite loops with zero-width matches
  if (m.index === regex.lastIndex) {
    regex.lastIndex++;
  }

  const target = `http://${m[1]}:${m[2]}`;
  const path = req.url.replace(`${m[1]}/${m[2]}/`, '')
  
  console.log(`${req.url} => ${target} :: ${path}`);
  req.url = path;
  proxy.web(req, res, { target: `${target}` });
});

proxy.on('error', function(err, req, res) {
  res.writeHead(500, {
    'Content-Type': 'text/plain'
  });

  res.end('Something went wrong. And we are reporting a custom error message.' + err);
});

console.log("listening on port 8080")
server.listen(8080);
