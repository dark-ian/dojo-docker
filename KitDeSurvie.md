# Dojo Docker
## Se connecter
* http://bit.ly/DarkLogin
* Login: `dojoXX`
* Mot de passe: `thalescom02`
## Kit de survie
https://gitlab.com/dark-ian/dojo-docker
## Exercice 1
* Créer un conteur
  * `docker create [OPTIONS] IMAGE [COMMAND] [ARG...]`
* Démarrer un conteneur
  * `docker start [OPTIONS] CONTAINER [CONTAINER...]`
* Créer & Démarrer un conteneur
  * `docker run [OPTIONS] IMAGE [COMMAND] [ARG...]`
* Lister les conteneurs
  * `docker ps [-a]`
* Redémarrer un conteneur
  * `docker restart CONTAINER [CONTAINER...]`
* Arrêter un conteneur
  * `docker stop [OPTIONS] CONTAINER [CONTAINER...]`
  * `docker kill [OPTIONS] CONTAINER [CONTAINER...]`
* Détruire un conteneur
  * `docker rm [OPTIONS] CONTAINER [CONTAINER...]`
 
## Exercice 2
* Pour plus de simplicité, vous pouvez partir de debian:stretch
* On peut trouver une liste des packages disponibles sur https://packages.debian.org/fr/ 

### Commandes Dockerfile

Les conteneurs sont créés à partir d’un Dockerfile: 

`docker build –t nom-image:tag-version ~/repertoire`

* `RUN`: On lance des commandes dans le container
* `ENTRYPOINT/CMD`: Le programme à lancer (remplace l’init)
* `COPY/ADD`: On ajoute des fichiers
* `EXPOSE`: Les ports exposés par les services de ce conteneur, utile pour l’automatisation

### Debian/Ubuntu

Pour ajouter des nouveaux packages:
* D'abord mettre à jour la liste des packages disponibles sur le système: `apt-get update`
* Si nécessaire, mettre à jour les packages déjà installés à leur dernière version: `apt-get dist-upgrade`
* Ensuite pour l'installation proprement dite: `apt-get install <package>`
 
### NGINX
* Sur Debian 9, le package s'appelle `nginx`
* Le binaire à lancer est `nginx`
** `CMD ["nginx", "-g", "daemon off;"]`
* Les fichiers de configurations sont dans `/etc/nginx`
* Par défaut les fichiers servis sont dans `/var/www/html`
* Accès depuis l’extérieur
  * Environnement Azure : Le port 80 est ouvert
  * Environnement AWS : Il y a un raccourci vers le port 8080, voir README.md dans les fichiers fournis

### Serveur HTTP Vert.X
* La seule dépendance nécessaire est java (package openjdk-8-jre-headless)
* La commande à lancer est `java -jar sample-1.0.0-SNAPSHOT-fat.jar -conf <chemin fichier de config>`
* Le binaire trace son port de démarrage. `docker logs [id]`
* Valeurs par défaut des endpoints REST : 
  * `curl http://<ip conteneur>:<port>/`
    * `<html><body><h1>Resource not found</h1></body></html>`
  * `curl http://<ip conteneur>:<port>/api/v1/polls`
    * `{ }`
 
## Exercice 2b
### Supervisord
Manuel sur http://supervisord.org/ 

### Fichier d’exemple :
~~~~
[program:jenkins_instance1]
command = java -jar jenkins-2.164.3.war --httpPort=8080
environment = JENKINS_HOME="/free/t0120397/Projects/AREOS/JENKINS2_HOME"
autorestart = true
[program:jenkins_instance2]
command = java -jar jenkins.war --httpPort=8081
environment = JENKINS_HOME="/free/t0120397/Projects/CTNG/JENKINS2_HOME"
autorestart = true
~~~~
